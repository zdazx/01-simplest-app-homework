package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiplyController {
    @GetMapping(value = "/api/tables/multiply")
    public String multiply(){
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < i + 1; j++) {
                sb.append(i + "*" + j + "=" + (i * j));
                if(Integer.toString(i * j).length() == 2) {
                    sb.append("&nbsp;&nbsp;");
                }else {
                    sb.append("&nbsp;&nbsp;&nbsp;");
                }
            }
            sb.append("<br>");
        }
        return sb.toString();
    }
}
