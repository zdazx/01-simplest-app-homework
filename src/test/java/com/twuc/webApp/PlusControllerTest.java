package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class PlusControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_status_200_code() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/plus")).andReturn();
        assertThat(mvcResult.getResponse().getStatus()).isEqualTo(200);
    }

    @Test
    void should_return_content_type_with_text_plain() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/tables/plus")).andReturn();
        assertThat(mvcResult.getResponse().getContentType().contentEquals("text/plain"));
    }
}
